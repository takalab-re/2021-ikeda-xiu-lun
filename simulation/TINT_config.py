import json

class SimulationConfig:
    def __init__(self, config_json_name):
        json_open = open(config_json_name,'r',encoding="utf-8")
        self.config = json.load(json_open)
