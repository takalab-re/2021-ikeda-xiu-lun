import pandas as pd
import itertools as iter

class DataLoader:
    def __init__(self,data_identifier):
        self.data_identifier  = data_identifier
        self.data_load_method = self.set_data_load_method()
        self.assoc_data,self.node_data = self.data_load_method(self.data_identifier)
        
    def set_data_load_method(self):
        if self.data_identifier in ["SINGLE","AND","METAPHOR"]:
            return load_three_prefix_data
        elif self.data_identifier in ["THREE"]:
            return load_three_metaphor_data

    def data_load(self):
        return self.assoc_data,self.node_data

    def init_nodes(self,center_image):
        node_data = self.node_data
        init_nodes = list(node_data[(node_data[0]==center_image) & (node_data[1]!=center_image)][1])
        return init_nodes

class TriDataLoader(DataLoader):
    def __init__(self,data_identifier):
        super().__init__(data_identifier)

    def tri_structures_all(self,image_list):
        return list(iter.permutations(image_list, 2))


# 3つの比喩の連想確率のデータをpandas DataFrame 形式で読み込んで返す
def load_three_metaphor_data():
    DIR = "./../Data/three_metaphor_data/"
    assoc_df = pd.read_csv(DIR+"three_metaphor_assoc_data.csv",header=0,index_col=0,encoding="utf-8")
    node_df = pd.read_csv(DIR+"three_metaphor_images.csv",header=None,index_col=None,names=(0,1),encoding="shift-jis")
    return assoc_df,node_df

def load_SINGLE_data():
    DIR = "./../Data/custom_difference_metaphar_data/"

    assoc_df = pd.read_csv(DIR+"custom_difference_metaphar_data.csv",header=0,names=("source","target","weight"))
    image_df = pd.read_csv(DIR+"all_images.csv",index_col=None,header=None,names=(0,1))
    return assoc_df,image_df


def load_AND_data():
    DIR = "./../Data/AND_METAPHOR_data/"
    assoc_df = pd.read_csv(DIR+"AND_metaphor_data.csv",header=0,names=("source","target","weight"))
    image_df = pd.read_csv(DIR+"AND_all_images.csv",header=None,index_col=None)

    return assoc_df,image_df

def load_METAPHOR_data():
    DIR = "./../Data/AND_METAPHOR_data/"

    assoc_df = pd.read_csv(DIR+"METAPHOR_metaphor_data.csv",header=0,names=("source","target","weight"))
    image_df = pd.read_csv(DIR+"METAPHOR_all_images.csv",header=None,index_col=None)
    return assoc_df,image_df

def load_three_prefix_data(prefix):
    if prefix == "SINGLE":
        return load_SINGLE_data()
    elif prefix == "AND":
        return load_AND_data()
    elif prefix == "METAPHOR":
        return load_METAPHOR_data()
    else:
        return None
