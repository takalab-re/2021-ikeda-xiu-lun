import os 

class SimulationRecorder:
    def __init__(self, target, source, config):
        self.save_dir = config["SaveDir"]
        self.corr_count_dict = dict()
        self.target = target
        self.source = source
        self.seed   = config["Seed"]
        self.prefix = config["DataPrefix"]
        self.sim_iter = config["SimIter"]

    # 1試行の結果を対応付けの回数をカウントする辞書に記録
    def record_functor(self, F_edge_dict):
        for B_edge, A_edge in F_edge_dict.items():
            if (B_edge, A_edge) in self.corr_count_dict:
                self.corr_count_dict[(B_edge, A_edge)] += 1
            else:
                self.corr_count_dict[(B_edge, A_edge)] = 1
    
    # 保存用のファイルの名前を作成
    def create_record_file_name(self):
        return self.save_dir + "Data_{}_seed_{}_{}_{}_correspondence.csv".format(self.prefix, self.seed, self.target, self.source)
    
    # 最終的な結果をcsvファイルに書き出す
    def all_dict_to_csv(self, remove_identity=False, is_na_count=True):
        fname = self.create_record_file_name()
        corr_sum_dict = {}
        with open(fname, 'w', encoding="utf-8") as f:
            f.write("B_dom,B_cod,A_dom,A_cod,count,probability\n")
            for key, count in self.corr_count_dict.items():

                B_edge, A_edge = key
                B_dom, B_cod = B_edge
                A_dom, A_cod = A_edge
                
                if remove_identity and B_dom == B_cod:
                    continue

                if B_edge in corr_sum_dict.keys():
                    corr_sum_dict[B_edge] += count
                else:
                    corr_sum_dict[B_edge] = count

                f.write("{0},{1},{2},{3},{4},{5}\n".format(B_dom, B_cod, A_dom, A_cod, count, count/self.sim_iter))
            
            if is_na_count:
                for key, value in corr_sum_dict.items():
                    B_dom, B_cod = key
                    f.write("{0},{1},{2},{3},{4},{5}\n".format(B_dom, B_cod, "NA", "NA", (self.sim_iter-value), (self.sim_iter-value)/self.sim_iter))

class TriSimulationRecorder(SimulationRecorder):
    def __init__(self, target, source, tri_dom, tri_cod, config):
        super().__init__(target, source, config)
        self.corr_type = config["CorrType"]
        self.is_embed  = config["IsEmbed"]
        self.tri_dom,self.tri_cod = tri_dom, tri_cod
        self.make_dir_if_not_exist()
        self.set_save_dir()

    def set_save_dir(self):
        if self.is_embed:
            self.save_dir = self.save_dir+"is_embed"

    def record_functor(self, F_edge_dict):
        for B_edge, A_edge in F_edge_dict.items():
            if (B_edge, A_edge) in self.corr_count_dict:
                self.corr_count_dict[(B_edge, A_edge)] += 1
            else:
                self.corr_count_dict[(B_edge, A_edge)] = 1
                
    def create_record_file_name(self):
        return self.save_dir + "Data_{}_{}_seed_{}_{}_{}_{}_{}_correspondence.csv".format(self.prefix, self.corr_type, self.seed, self.target, self.source, self.tri_dom, self.tri_cod)

    def make_dir_if_not_exist(self):
        if not os.path.exists(self.save_dir):
            os.makedirs(self.save_dir)

    def all_dict_to_csv(self, remove_identity=False, is_na_count=True):
        fname = self.create_record_file_name()
        corr_sum_dict = {}

        with open(fname, 'w', encoding="utf-8") as f:
            f.write("B_dom,B_cod,A_dom,A_cod,count,probability\n")
            for key, count in self.corr_count_dict.items():

                B_edge, A_edge = key
                B_dom, B_cod = B_edge
                A_dom, A_cod = A_edge
                
                if remove_identity and B_dom == B_cod:
                    continue

                if B_edge in corr_sum_dict.keys():
                    corr_sum_dict[B_edge] += count
                else:
                    corr_sum_dict[B_edge] = count

                f.write("{0},{1},{2},{3},{4},{5}\n".format(B_dom, B_cod, A_dom, A_cod, count, count/self.sim_iter))
            
            if is_na_count:
                for key, value in corr_sum_dict.items():
                    B_dom, B_cod = key
                    f.write("{0},{1},{2},{3},{4},{5}\n".format(B_dom, B_cod, "NA", "NA", (self.sim_iter-value), (self.sim_iter-value)/self.sim_iter))
    
class TriDevelpomentSimulationRecorder(SimulationRecorder):
    def __init__(self, target, source, config):
        super().__init__(target, source, config)
        self.corr_type = config["CorrType"]
        self.is_embed  = config["IsEmbed"]
        self.excitation_tri_count = dict()
        self.excitation_edge_count = dict()
        self.temperature = config["temperature"]
        self.set_save_dir()
        self.make_dir_if_not_exist()

    def set_save_dir(self):
        self.save_dir = self.save_dir+"temp {}/".format(self.temperature)

        if self.is_embed:
            self.save_dir = self.save_dir+"is_embed"
    
    def make_dir_if_not_exist(self):
        if not os.path.exists(self.save_dir):
            os.makedirs(self.save_dir)

    def create_record_file_name(self):
        return self.save_dir + "Data_{}_{}_seed_{}_{}_{}_correspondence.csv".format(self.prefix, self.corr_type, self.seed, self.target, self.source)

    def record_excitation_edge_count(self,edge):
        if edge in self.excitation_edge_count.keys():
            self.excitation_edge_count[edge] += 1
        else:
            self.excitation_edge_count[edge] = 1
    
    def record_excitation_edges_count(self,edges):
        for edge in edges:
            self.record_excitation_edge_count(edge)

    def record_excitation_tri_structure_count(self,tri_dom,tri_cod):
        if (tri_dom,tri_cod) in self.excitation_tri_count.keys():
            self.excitation_tri_count[(tri_dom,tri_cod)] += 1
        else:
            self.excitation_tri_count[(tri_dom,tri_cod)] =  1

    def record_excitation_tri_structures_count(self,tri_structures):
        for dom,cod in tri_structures:
            self.record_excitation_tri_structure_count(dom,cod)

    def excitation_tri_structures_to_csv(self):
        fname = self.save_dir+"{}_seed_{}_excitation_tri_count_{}_{}.csv".format(self.prefix,self.seed,self.target,self.source)
        with open(fname,'w',encoding="utf-8") as f:
            f.write("tri_dom,tri_cod,count\n")
            for key, count in self.excitation_tri_count.items():
                tri_dom,tri_cod = key
                f.write("{0},{1},{2}\n".format(tri_dom, tri_cod, count))

    def all_dict_to_csv(self, remove_identity=False, is_na_count=True):
        fname = self.create_record_file_name()
        corr_sum_dict = {}

        excitation_tri_count = self.excitation_tri_count.copy()
        excitation_edge_count = self.excitation_edge_count.copy()

        with open(fname, 'w', encoding="utf-8") as f:
            f.write("B_dom,B_cod,A_dom,A_cod,count,probability\n")
            for key, count in self.corr_count_dict.items():

                B_edge, A_edge = key
                B_dom, B_cod = B_edge
                A_dom, A_cod = A_edge
                
                if remove_identity and B_dom == B_cod:
                    continue

                if B_edge in excitation_edge_count.keys():
                    excitation_edge_count[B_edge] -= count
                if B_edge in excitation_tri_count.keys():
                    excitation_tri_count[B_edge] -= count

                f.write("{0},{1},{2},{3},{4},{5}\n".format(B_dom, B_cod, A_dom, A_cod, count, count/self.sim_iter))
            
            if is_na_count:              
                # コスライス圏の対象となる射が対応づかなかった回数について、書き込み
                for key,value in excitation_edge_count.items():
                    source,cod = key
                    if remove_identity and source != cod:
                        f.write("{0},{1},{2},{3},{4},{5}\n".format(source,cod,"NA","NA",value,value/self.excitation_edge_count[key]))

                # 三角構造の射が対応づかなかった回数について、書き込み
                for key,value in excitation_tri_count.items():
                    tri_dom,tri_cod = key                    
                    f.write("{0},{1},{2},{3},{4},{5}\n".format(tri_dom,tri_cod,"NA","NA",(value),(value)/self.excitation_tri_count[key]))
