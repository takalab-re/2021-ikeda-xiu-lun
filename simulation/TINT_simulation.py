import networkx as nx
import random 
import numpy as np
from tqdm import trange
from common import identity_morphism, edge_correspondence_dict
from TINT_rule import forced_anti_fork_rule, full_anti_fork_rule
from correspondence import ExcitedObjectCorrespondence,ExitedTriangleStructureCorrespondence,DevelopmentTriangleStructureCorrespondence

# 対象同士の対応付けのシミュレーションを実行するクラス
class ObjectCorrspondenceSimulation:
    def __init__(self, target, source, config, data_loader, recorder):
        self.target, self.source = target, source
        self.config = config
        self.recorder = recorder
        self.data_loader = data_loader
        # TODO:データの読み込み関数をここでしているすると結局書き換えないといけないので、外部指定かconfigをクラスにしてそこに追加する
        self.assoc_df, self.node_data = self.data_loader.data_load()
        self.assoc_net = self.init_assoc_net()
        self.target_nodes, self.source_nodes = self.data_loader.init_nodes(self.target), self.data_loader.init_nodes(self.source)
        self.anti_method = self.set_anti_method()

    # 緩和のルールを選択する
    def set_anti_method(self):
        anti_type = self.config["AntiType"]
        if anti_type == "forced":
            return forced_anti_fork_rule
        elif anti_type == "full":
            return full_anti_fork_rule

    # 対応付けをするクラスを作成
    def set_correspondence_class(self):
        return ExcitedObjectCorrespondence(
                self.assoc_net,self.target,self.source,
                self.target_nodes,self.source_nodes,
                self.est_target,self.est_source
            )

    # 潜在圏の作成
    def init_assoc_net(self):
        assoc_net = nx.from_pandas_edgelist(df = self.assoc_df,  source='source',  target='target', edge_attr=["weight"],  create_using=nx.DiGraph)
        identity_morphism(assoc_net) # 恒等射の追加
        return assoc_net

    # 射を励起するかどうかを判断するランダム値の生成に使用するseed値の設定
    def set_seed(self):
        seed = self.config["Seed"]
        random.seed(seed)
        np.random.seed(seed=seed)

    # 被喩辞側のコスライス圏の初期状態を設定
    def init_target_category(self):
        est_target = nx.DiGraph()
        for target_node in self.target_nodes:
            est_target.add_edge(self.target, target_node)
        identity_morphism(est_target)
        return est_target

    # 喩辞側のコスライス圏の初期状態を設定
    def init_source_category(self):
        est_source = nx.DiGraph()
        for source_node in self.source_nodes:
            est_source.add_edge(self.source, source_node)
        identity_morphism(est_source)
        return est_source

    # シミュレーションを実行する
    def run_simulation(self):
        self.set_seed()
        self.est_target,self.est_source = self.init_target_category(),self.init_source_category()
        self.correspondence_class = self.set_correspondence_class()
        for i in trange(self.config["SimIter"], desc="SIM_TIMES", leave=False):
            self.simulation()
        self.recorder.all_dict_to_csv(remove_identity=True, is_na_count=True)

    # 1回の対応付けを行う(run_simulationから規定回数呼び出される)
    def simulation(self):
        fork_edges, target_remain_edges, source_remain_edges, BMF_node_dict, F_node_dict = self.correspondence_class.search_correspondence()
        antied_est_target, antied_est_source = self.anti_method(self.est_target, self.est_source, self.target, fork_edges, target_remain_edges, source_remain_edges)
        BMF_edge_dict =  edge_correspondence_dict(antied_est_source, antied_est_target, BMF_node_dict)
        F_edge_dict   =  edge_correspondence_dict(antied_est_source, antied_est_target, F_node_dict)
        
        # 本当は関手になっているか、自然変換になっているかをしっかりチェックしないといけない
        # self.is_natural_transformation(BMF_node_dict,BMF_edge_dict,F_node_dict,F_edge_dict)        
        self.recorder.record_functor(F_edge_dict)

    def is_natural_transformation(self,BMF_node_dict,BMF_edge_dict,F_node_dict,F_edge_dict):
        pass

# 三角構造同士の対応付けのシミュレーションを実行するクラス
class TriCorrspondenceSimulation(ObjectCorrspondenceSimulation):
    def __init__(self, target, source, tri_dom, tri_cod, config, data_loader, recorder):
        super().__init__(target, source, config, data_loader, recorder)
        self.tri_dom,self.tri_cod = tri_dom,tri_cod
        self.tri_structure = (self.tri_dom,self.tri_cod)
        self.target_tri_structures = self.data_loader.tri_structures_all(self.target_nodes)

    def set_correspondence_class(self):
        corr_type = self.config["CorrType"]
        is_embed  = self.config["IsEmbed"]
        return ExitedTriangleStructureCorrespondence(
            self.assoc_net,self.target,self.source,self.tri_dom,self.tri_cod,
            self.target_nodes,self.source_nodes,
            self.est_target,self.est_source,
            corr_type,is_embed
            )

    def init_target_category(self):
        est_target = nx.DiGraph()
        for target_node in self.target_nodes:
            est_target.add_edge(self.target, target_node)
        
        for tri_dom,tri_cod in self.target_tri_structures:
            est_target.add_edge(tri_dom,tri_cod)

        identity_morphism(est_target)
        return est_target

    def init_source_category(self):
        est_source = nx.DiGraph()
        for source_node in self.tri_structure:
            est_source.add_edge(self.source, source_node)
        
        est_source.add_edge(self.tri_dom,self.tri_cod)
        identity_morphism(est_source)
        return est_source

    def simulation(self):
        fork_edges, target_remain_edges, source_remain_edges, BMF_node_dict, F_node_dict = self.correspondence_class.search_correspondence()
        antied_est_target, antied_est_source ,_ ,_ = self.anti_method(self.est_target, self.est_source, self.target, self.source, fork_edges, target_remain_edges, source_remain_edges, F_node_dict)
        BMF_edge_dict =  edge_correspondence_dict(antied_est_source, antied_est_target, BMF_node_dict)
        F_edge_dict   =  edge_correspondence_dict(antied_est_source, antied_est_target, F_node_dict)
        self.recorder.record_functor(F_edge_dict)

# やっぱりクラスを分離するべきかもしれない
# 初期化に必要な変数が異なるので、ここもクラスを分離する必要がありそう
class TriDevelopmentCorrspondenceSimulation(ObjectCorrspondenceSimulation):
    def __init__(self, target, source, config, data_loader, recorder):
        super().__init__(target, source, config, data_loader, recorder)
        self.temperature = config["temperature"]
        self.target_tri_structures = self.data_loader.tri_structures_all(self.target_nodes)

    def set_correspondence_class(self):
        corr_type = self.config["CorrType"]
        is_embed  = self.config["IsEmbed"]
        return DevelopmentTriangleStructureCorrespondence(
            self.assoc_net,self.target,self.source,self.source_tri_structures,
            self.est_target,self.est_source,
            corr_type,is_embed
            )

    def softmax(self,x):
        div_x = np.exp(np.array(x) / self.temperature)
        sum_x = np.sum(div_x)

        return div_x / sum_x

    def cal_triangles_assoc_weight(self):
        triangles = self.data_loader.tri_structures_all(self.source_nodes)
        triangle_weights = []
        for dom,cod in triangles:
            dom_weight = self.assoc_net[self.source][dom]["weight"]
            cod_weight = self.assoc_net[self.source][cod]["weight"]
            tri_weight = self.assoc_net[dom][cod]["weight"]
            triangle_weights.append(dom_weight+cod_weight+tri_weight)
        
        return triangles,triangle_weights 

    def select_stochastic(self,tri_strctures,softmax_weights):
        rand = random.random()
        prev_prob = 0
        for i,now_prob in enumerate(softmax_weights):
            if prev_prob <= rand <= (prev_prob + now_prob):
                return tri_strctures[i]
            prev_prob += now_prob

    def select_excite_tri_structure(self):
        tri_structures, tri_weight_sum_list = self.cal_triangles_assoc_weight()
        softmax_weights = self.softmax(tri_weight_sum_list)
        excite_tri_structure = self.select_stochastic(tri_structures,softmax_weights)
        
        return excite_tri_structure

    def init_source_category(self):
        self.source_tri_structures = []
        est_source = nx.DiGraph()
        tri_dom,tri_cod = self.select_excite_tri_structure()
        self.source_tri_structures.append((tri_dom,tri_cod))

        est_source.add_edge(self.source,tri_dom)
        est_source.add_edge(self.source,tri_cod)
        est_source.add_edge(tri_dom,tri_cod)
        identity_morphism(est_source)
        return est_source

    def softmax_select_addition_triangle(self):
        tri_dom,tri_cod = self.source_tri_structures[0]   # 一番最初に追加された三角構造を取得
        remain_source_nodes = [node for node in self.source_nodes if node != tri_dom and node != tri_cod]   # まだ励起されていない対象の取得

        weights = [self.assoc_net[tri_dom][new_cod]["weight"] for new_cod in remain_source_nodes]   # 最初に追加された三角構造(dom,cod)のdomからまだ励起されていない対象への連想確率を取得
        softmax_weights = self.softmax(weights)
        new_tri_cod = self.select_stochastic(remain_source_nodes,softmax_weights)   # sotfmaxを適用した連想確率から確率的に次に励起する対象を選択
        return new_tri_cod

    def add_tri_structure(self):
        add_tri_cod = self.softmax_select_addition_triangle()
        tri_dom,tri_cod = self.source_tri_structures[0]
        self.est_source.add_edge(self.source,add_tri_cod)
        self.est_source.add_edge(tri_dom,add_tri_cod)
        self.source_tri_structures.append((tri_dom,add_tri_cod))
        # identity_morphism(self.est_source)

    def run_simulation(self):
        self.set_seed()
        for i in trange(self.config["SimIter"], desc="SIM_TIMES", leave=False):
            self.est_target,self.est_source = self.init_target_category(),self.init_source_category()
            self.correspondence_class = self.set_correspondence_class()
            self.simulation()
        self.recorder.all_dict_to_csv(remove_identity=True, is_na_count=True)
        self.recorder.excitation_tri_structures_to_csv()


    def simulation(self):
        # 最初の三角構造の対応付けを探索する
        fork_edges, target_remain_edges, source_remain_edges, BMF_node_dict, F_node_dict = self.correspondence_class.search_correspondence()
        # antied_est_target, antied_est_source ,_ ,_ = self.anti_method(self.est_target, self.est_source, self.target, self.source, fork_edges, target_remain_edges, source_remain_edges, F_node_dict)
        # BMF_edge_dict =  edge_correspondence_dict(antied_est_source, antied_est_target, BMF_node_dict)
        # F_edge_dict   =  edge_correspondence_dict(antied_est_source, antied_est_target, F_node_dict)
        
        self.add_tri_structure()
        self.correspondence_class.update(self.est_target,self.est_source,self.source_tri_structures)


        fork_edges, target_remain_edges, source_remain_edges, BMF_node_dict, F_node_dict = self.correspondence_class.search_correspondence()
        antied_est_target, antied_est_source ,_ ,_ = self.anti_method(self.est_target, self.est_source, self.target, self.source, fork_edges, target_remain_edges, source_remain_edges, F_node_dict)
        BMF_edge_dict =  edge_correspondence_dict(antied_est_source, antied_est_target, BMF_node_dict)
        F_edge_dict   =  edge_correspondence_dict(antied_est_source, antied_est_target, F_node_dict)

        self.recorder.record_functor(F_edge_dict)

        # 今回の対応付けにて励起された三角構造と励起された射を記録しておく
        self.recorder.record_excitation_tri_structures_count(self.source_tri_structures)
        self.recorder.record_excitation_edges_count(self.est_source.edges)



    