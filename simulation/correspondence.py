import numpy as np
import random
from common import identity_morphism, edge_correspondence_dict


# 喩辞も被喩辞も励起が完了している場合の対象同士の対応付けを行うクラス
class ExcitedObjectCorrespondence:
    def __init__(self,assoc_net,target,source,target_nodes,source_nodes,est_target,est_source):
        self.assoc_net = assoc_net
        self.target = target 
        self.source = source
        
        # コスライス圏は確立していて動かないので、喩辞・被喩辞から連想する対象を持っておく
        # もしコスライス圏が動く場合は、ここでest_targetとest_sourceから取得する関数を追加するとかで
        self.target_nodes = target_nodes
        self.source_nodes = source_nodes
        self.est_target = est_target
        self.est_source = est_source

    # 対応付けの探索を行う
    def search_correspondence(self):
        # 行単位で一気に比較できるようにnumpy配列で連想確率と連想するかどうかを判断するランダム値を作成
        nt_weight_matrix = np.array([[self.assoc_net[s_node][t_node]["weight"] for t_node in self.target_nodes] for s_node in self.source_nodes])
        random_matrix = np.random.rand(len(self.source_nodes), len(self.target_nodes))

        fork_edges = set()
        target_remain_edges = set()
        source_remain_edges = set()
        BMF_node_dict = {self.source:self.target}
        F_node_dict   = {self.source:self.target}

        
        for i,  (rnd_list,  nt_weight_list) in enumerate(zip(random_matrix,  nt_weight_matrix)):
            # ここではrand_listとnt_weight_listを比較して、nt_weightが超えている部分だけTrueになる配列を作成(超えている＝連想した)
            # numpyでは、配列の引数に配列を指定できるので、target_nodesからTrueの部分だけを取り出す
            nt_cand_list = np.array(self.target_nodes)[rnd_list < nt_weight_list]                   #自然変換の候補の取得
            if nt_cand_list.size == 0:#候補が一つも励起されなかった場合飛ばす
                continue
            
            nt_cand_weights = nt_weight_list[rnd_list < nt_weight_list]         #候補の部分の重みを取得
            nt_idx_list = np.where(nt_cand_weights == np.max(nt_cand_weights))  #最大の重みのindexのリストを取得
            nt_idx = random.choice(nt_idx_list[0])                              #最大のものが複数あったときにはランダムで選択
        
            S_node,  T_node = self.source_nodes[i] ,  nt_cand_list[nt_idx]  #対応付けられる喩辞のイメージ、候補から選ばれた被喩辞側のイメージ  
            BMF_node_dict[S_node] = S_node                    #BMFの記録
            F_node_dict[S_node]   = T_node                    #Fの記録
            fork_edges.add((S_node,  T_node))                 #自然変換の要素を記録
            target_remain_edges.add((self.target, T_node))    #Fでうつされる射を記録
            source_remain_edges.add((self.source, S_node))    #BMF, Fのうつり元となる射を記録
    
        return fork_edges,  target_remain_edges,  source_remain_edges,  BMF_node_dict,  F_node_dict

# 喩辞も被喩辞も励起が完了している場合の三角構造同士の対応付けを行うクラス
class ExitedTriangleStructureCorrespondence(ExcitedObjectCorrespondence):
    def __init__(self,assoc_net,target,source,tri_dom,tri_cod,target_nodes,source_nodes,est_target,est_source,select_method="prob",is_embed=False):
        super().__init__(assoc_net,target,source,target_nodes,source_nodes,est_target,est_source)
        self.tri_dom = tri_dom
        self.tri_cod = tri_cod
        self.tri_structure = (self.tri_dom,self.tri_cod)
        self.select_method = select_method
        self.is_embed = is_embed
        self.select_correspondence_canditate_method = self.init_select_correspondence_canditate_method()

    # 対応付けの基準を選択する
    def init_select_correspondence_canditate_method(self):
        if self.select_method == "prob":
            return self.similar_prob_structure
        elif self.select_method == "ratio":
            return self.similar_ratio_structure

    def search_correspondence(self):
        #自然変換の探索で使う全ての重みを取得
        nt_weight_matrix = np.array([[self.assoc_net[s_node][t_node]["weight"] for t_node in self.target_nodes] for s_node in self.tri_structure])
        #重みと同じ形状のランダム値が入った配列を生成
        random_matrix = np.random.rand(len(self.tri_structure), len(self.target_nodes))

        # コスライス圏の射ごとに対応付けを行う
        # 現状喩辞はに1つの三角構造しか存在しないので、コスライス圏の射のdom,codに対して探している
        dom,cod = self.tri_dom,self.tri_cod
            
        # 連想されたイメージのリストの何番目にdom,codがあるかを取得(0,1でもよかったが、複数ある時のためにindexを探すようにした)
        dom_idx = np.where(np.array(self.tri_structure) == dom)[0][0]
        cod_idx = np.where(np.array(self.tri_structure) == cod)[0][0]

        dom_nt_weight = nt_weight_matrix[dom_idx]     # domから被喩辞の対象へ連想するかどうかの連想確率
        cod_nt_weight = nt_weight_matrix[cod_idx]     # codから被喩辞の対象へ連想するかどうかの連想確率
        dom_rnd_list  = random_matrix[dom_idx]        # domから被喩辞の対象へ連想するかどうかを決定するランダム値
        cod_rnd_list  = random_matrix[cod_idx]        # codから被喩辞の対象へ連想するかどうかを決定するランダム値

        # dom,codについて自然変換の要素の候補をとってくる
        dom_nt_cand_list = np.array(self.target_nodes)[dom_rnd_list < dom_nt_weight]    # dom_rnd_listのなかでdom_nt_weight未満の要素を取得（同じインデックスの要素同士で比較）
        cod_nt_cand_list = np.array(self.target_nodes)[cod_rnd_list < cod_nt_weight]    # cod_rnd_listのなかでcod_nt_weight未満の要素を取得（同じインデックスの要素同士で比較）

        # 候補の中から最も構造が類似しているものを自然変換の要素として選択
        # 選択する関数の中で、正しく関手になっていない候補は省かれる
        nt_tuple = self.select_correspondence_in_canditate(dom_nt_cand_list,cod_nt_cand_list)   # 重みの比

        return self.save_correspodence(dom,cod,nt_tuple)

    # 対応付けの候補の中から基準に従って1つを選ぶ
    def select_correspondence_in_canditate(self,dom_nt_cand_list, cod_nt_cand_list):
        correct_edge_nt_tuples = []
        tri_dom,tri_cod = self.tri_dom,self.tri_cod
        for dom_nt in dom_nt_cand_list:
            for cod_nt in cod_nt_cand_list:
                if not self.est_target.has_edge(dom_nt,cod_nt):
                    continue
                if self.is_embed and dom_nt == cod_nt: #現状は埋め込みが起こるような部分を省いて探す
                    continue
                
                dom_edge_weight   = self.assoc_net[self.source][tri_dom]["weight"]
                cod_edge_weight   = self.assoc_net[self.source][tri_cod]["weight"]
                tri_edge_weight   = self.assoc_net[tri_dom][tri_cod]["weight"]

                F_dom_edge_weight = self.assoc_net[self.target][dom_nt]["weight"]
                F_cod_edge_weight = self.assoc_net[self.target][cod_nt]["weight"]
                F_tri_edge_weight = self.assoc_net[dom_nt][cod_nt]["weight"]     

                structure_dist = self.select_correspondence_canditate_method(
                        dom_edge_weight   , cod_edge_weight  , tri_edge_weight,
                        F_dom_edge_weight , F_cod_edge_weight, F_tri_edge_weight
                        )
                correct_edge_nt_tuples.append( (dom_nt, cod_nt, structure_dist) )

        # 候補が1つもなければNoneを返す
        if len(correct_edge_nt_tuples) == 0:
            return None

        # 基準の差分が最も小さいものを選択する
        return self.min_edge_pair(correct_edge_nt_tuples)


    # 対応付けの候補の中で三角構造の連想確率の値が最も近い候補を選択する
    def similar_prob_structure(self, dom_edge_weight, cod_edge_weight, tri_edge_weight, F_dom_edge_weight, F_cod_edge_weight, F_tri_edge_weight):
        # 似た連想確率を持つ構造が一番移り先として適当なのではという予想
        weight_dist = abs(dom_edge_weight-F_dom_edge_weight) + abs(cod_edge_weight-F_cod_edge_weight) + abs(tri_edge_weight-F_tri_edge_weight) 
        return weight_dist


    # 対応付けの候補の中で三角構造の連想確立の比が最も近い候補を選択する
    def similar_ratio_structure(self, dom_edge_weight, cod_edge_weight, tri_edge_weight, F_dom_edge_weight, F_cod_edge_weight, F_tri_edge_weight):
        # 比の計算(現状はsource->domを1として他を計算する)
        dom_edge_ratio = dom_edge_weight / dom_edge_weight
        cod_edge_ratio = cod_edge_weight / dom_edge_weight
        tri_edge_ratio = tri_edge_weight / dom_edge_weight

        F_dom_edge_ratio = F_dom_edge_weight / F_dom_edge_weight
        F_cod_edge_ratio = F_cod_edge_weight / F_dom_edge_weight
        F_tri_edge_ratio = F_tri_edge_weight / F_dom_edge_weight

        # ソースの三角構造とターゲットの三角構造の構成要素同士の重みの比較
        # 似た連想確率を持つ構造が一番移り先として適当なのではという予想
        ratio_dist = abs(dom_edge_ratio - F_dom_edge_ratio) + abs(cod_edge_ratio-F_cod_edge_ratio) + abs(tri_edge_ratio - F_tri_edge_ratio) 
        return ratio_dist

    # 基準の中で最小値をとってきて、その最小値を持つ要素の中からランダムに変換を選択する
    def min_edge_pair(self,edge_correct_nt_pair):
        weights = [row[-1] for row in edge_correct_nt_pair]
        w_min = min(weights)

        min_cand = [row for row in edge_correct_nt_pair if row[-1] == w_min]
        idx_list = list(range(len(min_cand)))

        idx = np.random.choice(idx_list)
        return min_cand[idx]

    # 長かったので別の関数にした
    def save_correspodence(self,dom,cod,nt_tuple):
        #いま現状は痩せた圏なので集合型でいい
        fork_edges = set()  #自然変換
        target_rem_edges = set() #被喩辞側で残る射
        source_rem_edges = set() #喩辞側で対応付けられて残る射
        BMF_node_dict = {self.source:self.target}
        F_node_dict = {self.source:self.target}
        
        if nt_tuple == None:
            return fork_edges, target_rem_edges, source_rem_edges, BMF_node_dict, F_node_dict
        
        dom_nt,cod_nt,weight = nt_tuple

        # BMFの記録
        BMF_node_dict[dom] = dom
        BMF_node_dict[cod] = cod
        # Fの記録
        F_node_dict[dom] = dom_nt
        F_node_dict[cod] = cod_nt

        # 自然変換の要素になる射を記録
        fork_edges.add((dom,dom_nt))
        fork_edges.add((cod,cod_nt))

        # ターゲット側で残る射を記録
        target_rem_edges.add((self.target,dom_nt))
        target_rem_edges.add((self.target,cod_nt))
        target_rem_edges.add((dom_nt,cod_nt))
        target_rem_edges.add((dom,cod))          # BMFでのコスライス圏の射

        # ソース側で残る射を記録
        source_rem_edges.add((self.source,dom))
        source_rem_edges.add((self.source,cod))
        source_rem_edges.add((dom,cod))

        return fork_edges, target_rem_edges, source_rem_edges, BMF_node_dict, F_node_dict


# もしかすると継承せずにクラスを設計するべきなきがしてきた
# 持つべき変数とか初期化の方法が違うので、初期化の部分を別関数にするとかしないといけない
# TODO：もし継承したいのならば、source_nodesとtarget_nodesを初期化する関数を親クラスに追加して、継承先でオーバーライドできるようにするべき
# TODO：それかこれもObjectCorrespondenceから継承してしまえばいいのでは？
# めんどくさいので別クラスにしてしまおう
class DevelopmentTriangleStructureCorrespondence:
    def __init__(self, assoc_net, target, source, tri_structures, est_target, est_source, select_method="prob", is_embed=False):
        self.assoc_net = assoc_net
        self.target,self.source = target,source
        self.est_target,self.est_source = est_target, est_source
        self.source_nodes = [source_node for source_node in self.est_source.nodes if source_node != self.source]
        self.target_nodes = [target_node for target_node in self.est_target.nodes if target_node != self.target]
        self.select_method = select_method
        self.is_embed = is_embed
        self.select_correspondence_canditate_method = self.init_select_correspondence_canditate_method()

        # tri_structureを初期化すればいい気がしてきた
        # tri_dom,tri_cod → tri_structuresで渡す
        self.tri_structures = tri_structures
        self.F_node_dict = {self.source:self.target}
        self.BMF_node_dict = {self.source:self.target}
        self.fork_edges = set()
        self.target_rem_edges = set() #被喩辞側で残る射
        self.source_rem_edges = set() #喩辞側で対応付けられて残る射


    # 対応付けの基準を選択する
    def init_select_correspondence_canditate_method(self):
        if self.select_method == "prob":
            return self.similar_prob_structure
        elif self.select_method == "ratio":
            return self.similar_ratio_structure

    # 顕在圏の更新を行って情報の更新をする
    def update(self,est_target,est_source,tri_structure):
        self.est_source = est_source
        self.est_target = est_target
        self.source_nodes = [source_node for source_node in self.est_source.nodes if source_node != self.source]
        self.target_nodes = [target_node for target_node in self.est_target.nodes if target_node != self.target]
        self.tri_structures = tri_structure

    # 対応付けの探索を行う
    def search_correspondence(self):
        #自然変換の探索で使う全ての重みを取得
        nt_weight_matrix = np.array([[self.assoc_net[s_node][t_node]["weight"] for t_node in self.target_nodes] for s_node in self.source_nodes])
        #重みと同じ形状のランダム値が入った配列を生成
        random_matrix = np.random.rand(len(self.source_nodes), len(self.target_nodes))


        # コスライス圏の射(三角構造)ごとに対応付けを行う
        # ステップが違うなら、そもそもtri_structureを変更することで探索しないでもすむ可能性がある

        for dom,cod in self.tri_structures:             
            # 連想されたイメージのリストの何番目にdom,codがあるかを取得
            dom_idx = np.where(np.array(self.source_nodes) == dom)[0][0]
            cod_idx = np.where(np.array(self.source_nodes) == cod)[0][0]

            dom_nt_weight = nt_weight_matrix[dom_idx]     # domから被喩辞の対象へ連想するかどうかの連想確率
            cod_nt_weight = nt_weight_matrix[cod_idx]     # codから被喩辞の対象へ連想するかどうかの連想確率
            dom_rnd_list  = random_matrix[dom_idx]        # domから被喩辞の対象へ連想するかどうかを決定するランダム値
            cod_rnd_list  = random_matrix[cod_idx]        # codから被喩辞の対象へ連想するかどうかを決定するランダム値

            # dom,codについて自然変換の要素の候補をとってくる
            dom_nt_cand_list = np.array(self.target_nodes)[dom_rnd_list < dom_nt_weight]    # dom_rnd_listのなかでdom_nt_weight未満の要素を取得（同じインデックスの要素同士で比較）
            cod_nt_cand_list = np.array(self.target_nodes)[cod_rnd_list < cod_nt_weight]    # cod_rnd_listのなかでcod_nt_weight未満の要素を取得（同じインデックスの要素同士で比較）

            # 自然変換の候補をこれまでの対応付けにしがって絞る
            condtioned_dom_nt_cand_list,condtioned_cod_nt_cand_list = self.meet_to_conditions_triangles(dom,cod,dom_nt_cand_list,cod_nt_cand_list)

            # 候補の中から最も構造が類似しているものを自然変換の要素として選択
            # 選択する関数の中で、正しく関手になっていない候補は省かれる
            nt_tuple = self.select_correspondence_in_canditate(dom,cod,condtioned_dom_nt_cand_list,condtioned_cod_nt_cand_list)  # 重みの比
            self.save_correspodence(dom,cod,nt_tuple)

        return self.fork_edges, self.target_rem_edges, self.source_rem_edges, self.BMF_node_dict, self.F_node_dict

    # 対応付けの候補の中から基準に従って1つを選ぶ
    def select_correspondence_in_canditate(self,tri_dom,tri_cod,dom_nt_cand_list, cod_nt_cand_list):
        correct_edge_nt_tuples = []
        tri_dom,tri_cod = tri_dom,tri_cod
        for dom_nt in dom_nt_cand_list:
            for cod_nt in cod_nt_cand_list:
                if not self.est_target.has_edge(dom_nt,cod_nt):
                    continue
                if self.is_embed and dom_nt == cod_nt: #現状は埋め込みが起こるような部分を省いて探す
                    continue

                dom_edge_weight   = self.assoc_net[self.source][tri_dom]["weight"]
                cod_edge_weight   = self.assoc_net[self.source][tri_cod]["weight"]
                tri_edge_weight   = self.assoc_net[tri_dom][tri_cod]["weight"]

                F_dom_edge_weight = self.assoc_net[self.target][dom_nt]["weight"]
                F_cod_edge_weight = self.assoc_net[self.target][cod_nt]["weight"]
                F_tri_edge_weight = self.assoc_net[dom_nt][cod_nt]["weight"]     

                structure_dist = self.select_correspondence_canditate_method(
                        dom_edge_weight   , cod_edge_weight  , tri_edge_weight,
                        F_dom_edge_weight , F_cod_edge_weight, F_tri_edge_weight
                        )
                correct_edge_nt_tuples.append( (dom_nt, cod_nt, structure_dist) )

        # 候補が1つもなければNoneを返す
        if len(correct_edge_nt_tuples) == 0:
            return None

        # 基準の差分が最も小さいものを選択する
        return self.min_edge_pair(correct_edge_nt_tuples)

    # 対応付けの候補の中で三角構造の連想確率の値が最も近い候補を選択する
    def similar_prob_structure(self, dom_edge_weight, cod_edge_weight, tri_edge_weight, F_dom_edge_weight, F_cod_edge_weight, F_tri_edge_weight):
        # 似た連想確率を持つ構造が一番移り先として適当なのではという予想
        weight_dist = abs(dom_edge_weight-F_dom_edge_weight) + abs(cod_edge_weight-F_cod_edge_weight) + abs(tri_edge_weight-F_tri_edge_weight) 
        return weight_dist


    # 対応付けの候補の中で三角構造の連想確立の比が最も近い候補を選択する
    def similar_ratio_structure(self, dom_edge_weight, cod_edge_weight, tri_edge_weight, F_dom_edge_weight, F_cod_edge_weight, F_tri_edge_weight):
        # 比の計算(現状はsource->domを1として他を計算する)
        dom_edge_ratio = dom_edge_weight / dom_edge_weight
        cod_edge_ratio = cod_edge_weight / dom_edge_weight
        tri_edge_ratio = tri_edge_weight / dom_edge_weight

        F_dom_edge_ratio = F_dom_edge_weight / F_dom_edge_weight
        F_cod_edge_ratio = F_cod_edge_weight / F_dom_edge_weight
        F_tri_edge_ratio = F_tri_edge_weight / F_dom_edge_weight

        # ソースの三角構造とターゲットの三角構造の構成要素同士の重みの比較
        # 似た連想確率を持つ構造が一番移り先として適当なのではという予想
        ratio_dist = abs(dom_edge_ratio - F_dom_edge_ratio) + abs(cod_edge_ratio-F_cod_edge_ratio) + abs(tri_edge_ratio - F_tri_edge_ratio) 
        return ratio_dist

    # 基準の中で最小値をとってきて、その最小値を持つ要素の中からランダムに変換を選択する
    def min_edge_pair(self,edge_correct_nt_pair):
        weights = [row[-1] for row in edge_correct_nt_pair]
        w_min = min(weights)

        min_cand = [row for row in edge_correct_nt_pair if row[-1] == w_min]
        idx_list = list(range(len(min_cand)))

        idx = np.random.choice(idx_list)
        return min_cand[idx]

    # TODO:前のステップで対応付けられた対応付けを条件に現在の候補を絞る
    def meet_to_conditions_triangles(self,tri_dom,tri_cod,dom_nt_cand_list,cod_nt_cand_list):
        conditioned_dom_cand_list = dom_nt_cand_list
        conditioned_cod_cand_list = cod_nt_cand_list
        
        # すでに対応付けの辞書の中に存在すれば、変換の候補を対応付けられたもので固定
        # 最初の三角構造はここに引っかかることなく、すべての候補がここを通過する
        if tri_dom in self.F_node_dict.keys():
            correspondenced_dom_node = self.F_node_dict[tri_dom]
            conditioned_dom_cand_list = [correspondenced_dom_node] if correspondenced_dom_node in dom_nt_cand_list else []

        if tri_cod in self.F_node_dict.keys():
            correspondenced_cod_node = self.F_node_dict[tri_cod]
            conditioned_cod_cand_list = [correspondenced_cod_node] if correspondenced_cod_node in cod_nt_cand_list else []
        
        return conditioned_dom_cand_list,conditioned_cod_cand_list

    # 長かったので別の関数にした
    def save_correspodence(self,dom,cod,nt_tuple):
        #いま現状は痩せた圏なので集合型でいい
        if nt_tuple == None:
            return self.fork_edges, self.target_rem_edges, self.source_rem_edges, self.BMF_node_dict, self.F_node_dict
        
        dom_nt,cod_nt,weight = nt_tuple

        # BMFの記録
        self.BMF_node_dict[dom] = dom
        self.BMF_node_dict[cod] = cod
        # Fの記録
        self.F_node_dict[dom] = dom_nt
        self.F_node_dict[cod] = cod_nt

        # 自然変換の要素になる射を記録
        self.fork_edges.add((dom,dom_nt))
        self.fork_edges.add((cod,cod_nt))

        # ターゲット側で残る射を記録
        self.target_rem_edges.add((self.target,dom_nt))
        self.target_rem_edges.add((self.target,cod_nt))
        self.target_rem_edges.add((dom_nt,cod_nt))
        self.target_rem_edges.add((dom,cod))          # BMFでのコスライス圏の射

        # ソース側で残る射を記録
        self.source_rem_edges.add((self.source,dom))
        self.source_rem_edges.add((self.source,cod))
        self.source_rem_edges.add((dom,cod))
