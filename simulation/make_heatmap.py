from numpy import matrix
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os 

from TINT_data_loader import DataLoader,TriDataLoader

# 複数の比喩に対して対象同士の対応付けを行うクラス
class ObjectCorrspondenceHeatmapAll:
    def __init__(self,config):
        self.config = config.config

    def make_heatmap_each_metaphor(self):
        targets = self.config["Targets"]
        sources = self.config["Sources"]

        for target,source in zip(targets,sources):
            data_loader = DataLoader(self.config["DataPrefix"])
            heatmap = ObjectCorrespondenceHeatmap(target,source,self.config,data_loader)
            heatmap.make_correspondence_heatmap()

class TriDevelopmentCorrespondenceHeatmapAll(ObjectCorrspondenceHeatmapAll):
    def __init__(self, config):
        super().__init__(config)
    
    def make_heatmap_each_metaphor(self):
        targets = self.config["Targets"]
        sources = self.config["Sources"]

        for target,source in zip(targets,sources):
            data_loader = TriDataLoader(self.config["DataPrefix"])
            heatmap = TriCorrespondenceHeatmap(target,source,self.config,data_loader)
            heatmap.make_correspondence_heatmap()
            heatmap.make_excitation_heatmap()

class ObjectCorrespondenceHeatmap:
    def __init__(self, target, source, config, data_loader):
        plt.rcParams['font.family'] = "Meiryo"

        self.config = config
        self.data_loader = data_loader

        self.target = target
        self.source = source
        self.assoc_df, self.node_data = self.data_loader.data_load()
        self.target_nodes, self.source_nodes = self.data_loader.init_nodes(self.target), self.data_loader.init_nodes(self.source)

        self.corr_dir = config["SaveDir"]
        self.prefix   = config["DataPrefix"]
        self.seed     = config["Seed"]

    def set_save_dir(self):
        return "./heatmap/object/" # ここ外から渡すことになるかも

    def set_heatmap_name(self):
        return self.save_dir + "{}_seed_{}_object_edge_correspondence_count_{}_{}.png".format(self.prefix,self.seed,self.target,self.source)

    # 保存用のファイルの名前を作成
    def create_sim_file_name(self):
        return self.corr_dir + "Data_{}_seed_{}_{}_{}_correspondence.csv".format(self.prefix, self.seed, self.target, self.source)

    def make_dir_if_not_exist(self):
        if not os.path.exists(self.save_dir):
            os.makedirs(self.save_dir)

    def init_file_path(self):
        self.save_dir = self.set_save_dir()# ここ外から渡すことになるかも
        self.heatmap_name = self.set_heatmap_name()
        self.make_dir_if_not_exist()


    def make_correspondence_heatmap(self):
        self.init_file_path()

        sim_fname = self.create_sim_file_name()
        corr_data = pd.read_csv(sim_fname,header=0)
        corr_data = corr_data.fillna("NA") 

        corr_matrix = self.count_corrsopndence(corr_data)

        df = pd.DataFrame(corr_matrix, index=self.source_nodes, columns=self.target_nodes+["NA"])
        plt.clf()
        sns.heatmap(df,vmin=0.0,vmax=1000,cmap="Blues",
            linewidths=1,cbar=True,xticklabels=True,yticklabels=True,annot=True,fmt="d")
        plt.ylim(0,8)
        plt.yticks(rotation=0)
        plt.xticks(rotation=25)
        plt.savefig(self.heatmap_name)

    def count_corrsopndence(self, corr_data):
        edge_corr_dict = {(s_node,t_node):0 for t_node in self.target_nodes+["NA"] for s_node in self.source_nodes}

        # 喩辞の各対象について、被喩辞の度の対象に対応づいたかをカウント
        for s_node in self.source_nodes:
            corr_A_nodes = corr_data[corr_data["B_cod"]==s_node]
            for corr_A in corr_A_nodes.itertuples():
                count = corr_A.count
                edge_corr_dict[(s_node,corr_A.A_cod)] = count

        # カウント辞書になっている部分を行列形式に変換する
        matrix = list()
        for s_node in self.source_nodes:
            row = list()
            for t_node in self.target_nodes+["NA"]:
                row.append(edge_corr_dict[(s_node,t_node)])
            matrix.append(row)

        return matrix

class TriCorrespondenceHeatmap(ObjectCorrespondenceHeatmap):
    def __init__(self, target, source, config, data_loader):
        super().__init__(target, source, config, data_loader)
        self.corr_type = config["CorrType"]
        self.is_embed  = config["IsEmbed"]
        self.temperature = config["temperature"]

    def init_file_path(self):
        self.save_dir = self.set_save_dir()# ここ外から渡すことになるかも
        self.corr_dir = self.set_corr_dir()
        self.heatmap_name = self.set_heatmap_name()

        self.make_dir_if_not_exist()
        self.excitation_fname = self.set_excitation_name()
        self.excitation_heatmap_name = self.set_excitation_heatmap_name()

    def set_save_dir(self):
        save_dir = "./heatmap/Multi/temp {}/".format(self.temperature)

        if self.is_embed:
            save_dir = save_dir+"is_embed/"

        return save_dir

    def set_corr_dir(self):
        corr_dir = self.corr_dir+"temp {}/".format(self.temperature)

        if self.is_embed:
            corr_dir = self.corr_dir+"is_embed/"

        return corr_dir

    def create_sim_file_name(self):
        return self.corr_dir + "Data_{}_{}_seed_{}_{}_{}_correspondence.csv".format(self.prefix, self.corr_type, self.seed, self.target, self.source)

    def set_excitation_name(self):
        return self.corr_dir + "{}_seed_{}_excitation_tri_count_{}_{}.csv".format(self.prefix,self.seed,self.target,self.source)

    def set_excitation_heatmap_name(self):
        return self.save_dir + "{}_seed_{}_excitation_tri_count_{}_{}.png".format(self.prefix,self.seed,self.target,self.source)

    def make_correspondence_heatmap(self):
        self.init_file_path()
        sim_fname = self.create_sim_file_name()
        corr_data = pd.read_csv(sim_fname,header=0)
        corr_data = corr_data.fillna("NA") 

        corr_matrix = self.count_corrsopndence(corr_data)

        df = pd.DataFrame(corr_matrix, index=self.source_nodes, columns=self.target_nodes+["NA"])
        plt.clf()
        sns.heatmap(df,vmin=0.0,vmax=1000,cmap="Blues",
            linewidths=1,cbar=True,xticklabels=True,yticklabels=True,annot=True,fmt="d")
        plt.ylim(0,8)
        plt.yticks(rotation=0)
        plt.xticks(rotation=25)
        plt.savefig(self.heatmap_name)

    def count_corrsopndence(self, corr_data):
        edge_corr_dict = {(s_node,t_node):0 for t_node in self.target_nodes+["NA"] for s_node in self.source_nodes}

        # 喩辞の各対象について、被喩辞の度の対象に対応づいたかをカウント
        for s_node in self.source_nodes:
            corr_A_nodes = corr_data[(corr_data["B_cod"]==s_node) & (corr_data["B_dom"] == self.source)]
            for corr_A in corr_A_nodes.itertuples():
                count = corr_A.count
                edge_corr_dict[(s_node,corr_A.A_cod)] = count

        # カウント辞書になっている部分を行列形式に変換する
        matrix = list()
        for s_node in self.source_nodes:
            row = list()
            for t_node in self.target_nodes+["NA"]:
                row.append(edge_corr_dict[(s_node,t_node)])
            matrix.append(row)

        return matrix

    def make_excitation_heatmap(self):
        excitation_df = pd.read_csv(self.excitation_fname,header=0)

        corr_matrix = self.excitation_count(excitation_df)

        df = pd.DataFrame(corr_matrix, index=self.source_nodes, columns=self.source_nodes)
        plt.clf()
        sns.heatmap(df,vmin=0.0,vmax=1000,cmap="Blues",
            linewidths=1,cbar=True,xticklabels=True,yticklabels=True,annot=True,fmt="d")
        plt.ylim(0,8)
        plt.yticks(rotation=0)
        plt.xticks(rotation=25)
        plt.savefig(self.excitation_heatmap_name)


    def excitation_count(self,excitation_df):
        excitation_count = {(tri_dom,tri_cod):0 for tri_dom in self.source_nodes for tri_cod in self.source_nodes}

        for s_node in self.source_nodes:
            excitation_row = excitation_df[(excitation_df["tri_dom"]==s_node)]
            for row in excitation_row.itertuples():
                count = row.count
                excitation_count[(s_node,row.tri_cod)] = count

        matrix = list()
        for tri_dom in self.source_nodes:
            row = list()
            for tri_cod in self.source_nodes:
                row.append(excitation_count[(tri_dom,tri_cod)])
            matrix.append(row)
        return matrix
