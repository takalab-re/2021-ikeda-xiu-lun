import argparse
from make_heatmap import ObjectCorrspondenceHeatmapAll, TriDevelopmentCorrespondenceHeatmapAll
from TINT_config import SimulationConfig

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="TINTのシミュレーションを実行する")
    parser.add_argument("sim_type",help="対象同士のシミュレーションか三角構造同士のシミュレーションか")
    parser.add_argument("config_file_path",help="シミュレーションの設定ファイル")
    args = parser.parse_args()

    if args.sim_type == "obj":
        config   = SimulationConfig(args.config_file_path)
        heatmaps = ObjectCorrspondenceHeatmapAll(config)
        heatmaps.make_heatmap_each_metaphor()
    elif args.sim_type == "tri":
        config   = SimulationConfig(args.config_file_path)

    elif args.sim_type == "dev":
        config   = SimulationConfig(args.config_file_path)
        heatmaps = TriDevelopmentCorrespondenceHeatmapAll(config)
        heatmaps.make_heatmap_each_metaphor()

  