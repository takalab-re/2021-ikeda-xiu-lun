import argparse
from simulations import ObjectCorrspondenceSimulationAll,TriCorrspondenceSimulationAll,TriDevelopmentCorrespondenceSimulationAll
from TINT_config import SimulationConfig


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="TINTのシミュレーションを実行する")
    parser.add_argument("sim_type",help="対象同士のシミュレーションか三角構造同士のシミュレーションか")
    parser.add_argument("config_file_path",help="シミュレーションの設定ファイル")
    args = parser.parse_args()

    if args.sim_type == "obj":
        config   = SimulationConfig(args.config_file_path)
        simulator_all = ObjectCorrspondenceSimulationAll(config)
        simulator_all.simulation_each_metaphor()
    elif args.sim_type == "tri":
        config   = SimulationConfig(args.config_file_path)
        simulator_all = TriCorrspondenceSimulationAll(config)
        simulator_all.simulation_each_metaphor()
    elif args.sim_type == "dev":
        config   = SimulationConfig(args.config_file_path)
        simulator_all = TriDevelopmentCorrespondenceSimulationAll(config)
        simulator_all.simulation_each_metaphor()
  