from tqdm import tqdm

from TINT_simulation import ObjectCorrspondenceSimulation,TriCorrspondenceSimulation,TriDevelopmentCorrspondenceSimulation
from TINT_recorder import SimulationRecorder,TriSimulationRecorder,TriDevelpomentSimulationRecorder
from TINT_data_loader import DataLoader,TriDataLoader

# 複数の比喩に対して対象同士の対応付けを行うクラス
class ObjectCorrspondenceSimulationAll:
    def __init__(self,config):
        self.config = config.config

    def simulation_each_metaphor(self):
        targets = self.config["Targets"]
        sources = self.config["Sources"]

        for target,source in zip(targets,sources):
            recorder    = SimulationRecorder(target,source,self.config)
            data_loader = DataLoader(self.config["DataPrefix"])
            simulator   = ObjectCorrspondenceSimulation(target,source,self.config,data_loader,recorder)
            
            simulator.run_simulation()

# 複数の比喩に対して三角構造同士の対応付けを行うクラス
class TriCorrspondenceSimulationAll(ObjectCorrspondenceSimulationAll):
    def __init__(self,config):
        super().__init__(config)

    def simulation_each_metaphor(self):
        targets = self.config["Targets"]
        sources = self.config["Sources"]

        for target,source in zip(targets,sources):
            data_loader = TriDataLoader(self.config["DataPrefix"])
            source_nodes = data_loader.init_nodes(source)
            source_tri_structures = data_loader.tri_structures_all(source_nodes)

            for tri_dom,tri_cod in tqdm(source_tri_structures,desc="TRI NUM",leave=False):
                recorder    = TriSimulationRecorder(target,source,tri_dom,tri_cod,self.config)
                simulator   = TriCorrspondenceSimulation(target,source,tri_dom,tri_cod,self.config,data_loader,recorder)
                simulator.run_simulation()

# 複数の比喩に対して三角構造を発展させながら対応付けを行うクラス
class TriDevelopmentCorrespondenceSimulationAll(TriCorrspondenceSimulationAll):
    def __init__(self, config):
        super().__init__(config)
    
    def simulation_each_metaphor(self):
        targets = self.config["Targets"]
        sources = self.config["Sources"]

        for target,source in zip(targets,sources):
            data_loader = TriDataLoader(self.config["DataPrefix"])
            recorder    = TriDevelpomentSimulationRecorder(target,source,self.config)
            simulator   = TriDevelopmentCorrspondenceSimulation(target,source,self.config,data_loader,recorder)
            simulator.run_simulation()
